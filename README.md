# PCB Hello World Tutorial

Voltera PCB Machine  ‘Hello World’ Tutorial

PCB machines help to print circuit boards quickly, effectively and easily. The PCB we use is made by Voltera. This tutorial will take you through how to print the ‘Hello World’ PCB.

Printer Setup
To begin, open the Voltera software on your computer and select ‘Print’ then select ‘Simple’. 

For the board, make sure the small Voltera logo and pin are right-side up. Next, you’ll need to select what ink you’re using. 

![](PCB_p4.png)
 
The name and type of the ink can be found on the cartridge. 

**Printing the circuit**

Now, to load the ‘Hello World’ circuit, select ‘Hello World’ on the window to load prior circuits. 

The next step is to mount the board to the machine.

The clamps have two sides: the one with notches are for the substrate, the smooth one is for holding the printer board. So, for these purposes use the smooth side. 

![](PCB_p3.png)
 
As can be seen in the image above, place the top clamp on the fourth hole down and the bottom clamp on the fifth hole up. Ensure that the board is fit securely under the clamps. 

Now, the machine must calibrate itself using the aligning probe. The aligning probe is the one with the pointed tip and green contact pads on the top. Place the probe on the mount using the magnets and make sure the probe fits securely. 

Next, the probe has to align itself on where to print the circuit. On the voltera software, place approximately where you’d like the circuit to go. The voltera machine will outline that area on the machine so ensure that this area is centered on the board. 

Press the ink into the probe and screw a tip onto the dispenser. Next turn the knob at the top of the dispenser until ink is just coming out of the tip. It is important not to turn the knob too much or else ink will leak and if the knob is not turned enough, ink will not dispense. 

Then, mount the probe onto the dispenser and start the calibration. The lines it prints should look like this:
 
![](PCB_p2.png)

You may have to change the Z and E values. The Z number changes how close to the board the dispenser will dispense the ink. The E values will turn the calibration knob at the top of the dispenser, changing how much ink is dispensed. 

Once the calibration process is finished, the board can print. Be sure all the lines don’t appear too thin and enough ink dispenses. 

In the event of a certain portion not printing well, such as the ‘H’ in the ‘Hello World’, click and drag to highlight that portion and you can reprint that region. 

Next, carefully remove the board. Flip the clamps over and carefully place the board with the ink side down. The ‘Bake’ process will begin to thermally cure the board. 

Afterwards, use a wipe to clean off the excess ink. 

**Dispensing the Solder Paste**

The next step is to clamp the board down to dispense the thermal paste on the board. 

To align the circuit, select a region of the board and move the probe generally over the area you’d like to align. When you get closer to the area, hit the ‘Lower’ button until it’s perfectly centered. When the probe gets to the center, select ‘Measure’. Do this for each point on the board. 

Grab the solder paste and follow the same process to fill the probe, screw on the tip, and dispense the ink to the proper amount. Be sure to take time in this process and ensure that enough paste is being dispensed. 

![](PCB_p1.png)
 

The process will take some time to fully dispense enough ink, but the final product should like the image above. 

**Reflow and Assembly**

Finally, the components can be placed onto the boards. Carefully place each part onto the board in the corresponding locations. 

Now, the board is ready to reflow. This can take about 15-20 minutes, but your board is complete! 

